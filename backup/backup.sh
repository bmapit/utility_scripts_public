#!/usr/bin/env bash
set -e # better safe than sorry
set -u # same

# contact: jpierce@bmap.ucla.edu


########### CONFIGURABLE VARIABLES ###########
filename_prefix=foo 		# avoid spaces and special characters
filename_date=$(date +%Y-%m-%d) # avoid spaces and underscores

backup_root=/tmp
logfile="/var/log/${filename_prefix}_backup-shipper.log"
failure_email=foo@bar.org
##############################################

# redirect stdout and stderr to FDs 3 and 4, then to disk
exec 3>&1 4>&2 >>"${logfile}" 2>&1

# exit handling
cleanup() {
  # restore stdout and stderr, destroy FDs 3 and 4
  exec 1>&3 2>&4 3>&- 4>&-
}
trap cleanup INT QUIT TERM EXIT

echo "Hold onto your butts... beginning backup at $(date +%Y-%m-%d-%H:%M) ..." 

############## BACKUP FUNCTIONS ##############
source $(dirname "${BASH_SOURCE[0]}")/backup_mysql.sh
source $(dirname "${BASH_SOURCE[0]}")/backup_pgsql.sh
##############################################

# error handling
die() {
  local parent_lineno=$1
  local message=$2   # optional, therefore this might be unset
  local code=${3:-1} # optional, exit status defaults to "catchall for general errors" if unsupplied
  if [[ -n "$message" ]]; then
    echo "Error on or around line ${parent_lineno}: ${message}; exiting with status ${code}"
  else
    echo "Error on or around line ${parent_lineno}: exiting with status ${code}"
  fi

  # send an alert e-mail (for literally any command that fails)
  cat "${logfile}" | mailx -s "[coppolalab.ucla.edu] backup failure" ${failure_email}
  exit "${code}"
}
trap 'die ${LINENO}' ERR

case $1 in
  'all')
    backup_mysql "${backup_root}";
    backup_pgsql "${backup_root}";
#    backup_mongodb;
#    create_remote_copy;
    ;;

  'mysql')
    backup_msyql "${backup_root}";
    ;;

  'pgsql')
    backup_pgsql "${backup_root}";
    ;;

  'mongodb')
    backup_mongodb;
    ;;

  'rsync')
#    create_remote_copy;
    ;;

  *)
    echo "Usage: $0 [all|mysql|pgsql|mongodb|rsync]"
    ;;
esac
