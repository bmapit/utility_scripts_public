backup_pgsql() {
  ########### CONFIGURABLE VARIABLES ###########
  local psql_command=/usr/bin/pg_dumpall
  local psql_user=postgres
  local pgsql_backup_root=$1/pgsql
  local dump_file="${pgsql_backup_root}/${filename_prefix}_pgsqldb_${filename_date}.psql"
  ##############################################
  echo -e "\nBeginning pg_dumpall at $(date +%Y-%m-%d-%H:%M) ..."

  # sanity checks
  if [[ ! -d ${pgsql_backup_root} ]]; then
    die ${LINENO} "The pgsql backup directory is missing" 4
  elif [[ $(ls -ld "${pgsql_backup_root}" | awk '{print $3}') != postgres ]]; then
    die ${LINENO} "The pgsql backup is not owned by the psql superuser" 4
  fi

  # run a vanilla pg_dumpall
  su - ${psql_user} -c "${psql_command} >\"${dump_file}\"" || \
    die ${LINENO} "The su - ${psql_user} -c ... command failed" 4

  # report success or failure
  if [[ ! -f ${dump_file} ]]; then
    die ${LINENO} "The .psql file doesn't exist after the pgdump" 4
  elif [[ ! -s ${dump_file} ]]; then
    die ${LINENO} "The .psql file is empty" 4
  else
    echo "pg_dumpall completed successfully at $(date +%Y-%m-%d-%H:%M)"
  fi
}
