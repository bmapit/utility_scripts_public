backup_mysql() {
  ########### CONFIGURABLE VARIABLES ###########
  local mysql_command=/usr/bin/mysqldump
  # TODO create dedicated mysqldump user
  local mysql_user=root
  # TODO Consider moving to http://square.github.io/keywhiz , just realize it's
  # pointless if all the webapps are still storing passwords in plaintext.
  # Alternatively, just move into my.cnf if you're feeling bold
  local mysql_pass='password'
  
  local mysql_backup_root=$1/mysql
  local dump_file="${mysql_backup_root}/${filename_prefix}_mysqldb_${filename_date}.sql"
  ##############################################
  echo -e "\nBegining mysqldump at $(date +%Y-%m-%d-%H:%M) ..."


  # sanity checks
  [[ -d ${mysql_backup_root} ]] || die ${LINENO} "The mysql backup directory is missing" 3b

  # assume some random application works on MyISAM tables across multiple
  # schemas, and lock all tables for a consistent backup:
  ${mysql_command} --all-databases --lock-all-tables --flush-privileges \
    --routines --events -u"${mysql_user}" -p"${mysql_pass}" >"${dump_file}" || \
     die ${LINENO} "The mysqldump failed" 4
  # TODO incremental backups?

  # report success or failure
  if [[ ! -f ${dump_file} ]]; then
    die ${LINENO} "The .sql file doesn't exist after the mysqldump" 3
  elif [[ ! -s ${dump_file} ]]; then
    die ${LINENO} "The .sql file is empty" 3
  else
    echo "mysqldump completed successfully at $(date +%Y-%m-%d-%H:%M)"
 fi 
}
