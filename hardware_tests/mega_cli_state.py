#!/usr/bin/env python
# 3/9/2015
"""ndo@bmap.ucla.edu"""
"""sends an e-mail once every 24h if the output of /opt/MegaRAID/MegaCli/MegaCli64 -LDInfo -Lall -aAll does not show "Optimal" for any logical drive."""
import shlex
from subprocess import Popen, PIPE, call
import re
import os, time

#admin stuff:
contact_email = "foo@bar.org"

#helper functions:
def get_last_alert_time():
    """
    Get last alert time
    """
    if not os.path.isfile("/tmp/last_alert"): #if last_alert doesn't exist
        open('/tmp/last_alert', 'w').close() #then create it
        return 0 #last_alert = os.path.getmtime("/tmp/last_alert") - 86460 #modified time - 24h1'
    else:
        last_alert = os.path.getmtime("/tmp/last_alert")
    return last_alert


def get_exitcode_stdout_stderr(cmd):
    """
    Execute the external command and get its exitcode, stdout and stderr.
    """
    args = shlex.split(cmd)
    proc = Popen(args, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    exitcode = proc.returncode
    return exitcode, out, err

def get_megacli():
    exitcode, out, err = get_exitcode_stdout_stderr("which megacli")
    if exitcode == 0:
        return "megacli -LDInfo -Lall -aAll"
    else:
        return "/opt/MegaRAID/MegaCli/MegaCli64 -LDInfo -Lall -aAll"

#main:
try:
    cmd = get_megacli()
    exitcode, out, err = get_exitcode_stdout_stderr(cmd)

    alert_admin = False
    if exitcode == 0:
        states = re.findall(r'(State.+)', out)

        for state in states:
            if "Optimal" not in state:
                print 'The output of /opt/MegaRAID/MegaCli/MegaCli64 -LDInfo -Lall -aAll on icnn1.icnn.ucla.edu does not show "Optimal" for some logical drive.'
                alert_admin = True
                open('/tmp/last_known_bad_state','w').close()

            else:
                print "OK"
                open('/tmp/last_known_good_state','w').close()

    else:
        print err
        

    if alert_admin == True:
        if (time.time() - 82800) > get_last_alert_time(): #email once every 24h
            call("echo 'The output of /opt/MegaRAID/MegaCli/MegaCli64 -LDInfo -Lall -aAll on icnn1.icnn.ucla.edu does not show Optimal for some logical drive.' | mail -s 'ALERT: Some drive is not optimal on icnn1' %s" % (contact_email), shell=True)
            open('/tmp/last_alert','w').close()
except Exception as e:
    print e
    script_path = os.path.realpath(__file__)
    os.system("echo '%s: %s' | mail -s 'ALERT: Error on icnn1' %s" % (script_path, e, contact_email))